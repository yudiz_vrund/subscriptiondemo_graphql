import { gql } from '@apollo/client';

export const GET_USERS = gql`
  query get_users {
    users {
      id
      name
      email
    }
  }
`;

export const GET_MESSAGES = gql`
  query get_msg {
    messages {
      id
      message
      senderMail
      receiverMail
    }
  }
`;
