import { gql } from '@apollo/client';

export const NEW_USER_SUBSCRIPTION = gql`
  subscription NEW_USER {
    newUser {
      name
      email
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription Subscription($newMessageReceiverMail: String!) {
    newMessage(receiverMail: $newMessageReceiverMail) {
      message
      senderMail
      receiverMail
    }
  }
`;
