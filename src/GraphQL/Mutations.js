import { gql } from '@apollo/client';

export const CREATE_USER = gql`
  mutation CreateUserMutation(
    $createUserName: String!
    $createUserEmail: String!
  ) {
    createUser(name: $createUserName, email: $createUserEmail) {
      id
      __typename
      name
      email
    }
  }
`;

export const SEND_MSG = gql`
  mutation CreateMessageMutation(
    $createMessageSenderMail: String!
    $createMessageReceiverMail: String!
    $createMessageMessage: String!
  ) {
    createMessage(
      senderMail: $createMessageSenderMail
      receiverMail: $createMessageReceiverMail
      message: $createMessageMessage
    ) {
      id
      __typename
    }
  }
`;

export const DEL_MSG = gql`
  mutation DeleteMessageMutation($deleteMessageId: String!) {
    deleteMessage(id: $deleteMessageId)
  }
`;

export const DEL_USER = gql`
  mutation DeleteUserMutation($deleteUserEmail: String!) {
    deleteUser(email: $deleteUserEmail)
  }
`;
