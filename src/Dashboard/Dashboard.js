import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_USERS, GET_MESSAGES } from '../GraphQL/Queries';

import Users from '../Components/Users/Users';
import { Createuser } from '../Components/Createuser';
import Sendmsg from '../Components/Sendmsg';
import Messages from '../Components/Messages/Messages';

const Dashboard = () => {
  const { loading, error, data } = useQuery(GET_USERS);
  const { data: msgData } = useQuery(GET_MESSAGES);

  const show_msg = msgData?.messages;

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  const users_data = data.users;

  return (
    <>
      <div className="container-fluid mt-3">
        <div class="row">
          <div class="col-md-2">
            <Users data={users_data} />
          </div>
          <div class="col-md-6">
            <div class="row">
              <Createuser />
              <div className="col-md-1"></div>
              <Sendmsg />
            </div>
          </div>
          <div class="col">
            <Messages data={show_msg} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
