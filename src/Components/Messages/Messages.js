import React from 'react';
import { FaTrash } from 'react-icons/fa';
import { DEL_MSG } from '../../GraphQL/Mutations';
import { useMutation } from '@apollo/client';
import { GET_MESSAGES } from '../../GraphQL/Queries';

const Messages = (props) => {
  const show_msg = props.data;

  const [DELMESSAGE] = useMutation(DEL_MSG);

  const deletemsg = (id) => {
    DELMESSAGE({
      variables: {
        deleteMessageId: id,
      },
      refetchQueries: [{ query: GET_MESSAGES }],
    });
  };

  


  return (
    <>
      <div className="border p-2">
        <h6>Messages</h6>
        {show_msg?.map((msg) => {
          return (
            <>
              <div className="card mb-2 p-2">
                <div className="d-flex justify-content-start">
                  Sender Mail : {msg.senderMail}
                </div>
                <span></span>
                <span className="text-success d-flex justify-content-between">
                  {msg.message}{' '}
                  <span className="text-danger">
                    <FaTrash
                      onClick={() => {
                        deletemsg(msg.id);
                      }}
                    />{' '}
                  </span>
                </span>
                <div className="d-flex justify-content-start">
                  Receiver Mail : {msg.receiverMail}
                </div>
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};

export default Messages;
