import React from 'react';
import { FaTrash, FaEdit } from 'react-icons/fa';
import { DEL_USER } from '../../GraphQL/Mutations';
import { useMutation } from '@apollo/client';
import { GET_USERS } from '../../GraphQL/Queries';

const Users = (props) => {
  const users_data = props.data;

  const [DELUSER] = useMutation(DEL_USER);

  const deleteuser = (id) => {
    DELUSER({
      variables: {
        deleteUserEmail: id,
      },
      refetchQueries: [{ query: GET_USERS }],
    });
  };

  return (
    <>
      <div className="border p-2">
        <h6>Users</h6>
        {users_data.map((user) => {
          return (
            <>
              <div className="card mb-2 p-2">
                <span className="d-flex justify-content-between">
                  {user.name}
                  <span className="text-primary">
                    <FaEdit onClick={() => {}} />{' '}
                  </span>
                </span>
                <span className="text-success d-flex justify-content-between">
                  {user.email}
                  <span className="text-danger">
                    <FaTrash
                      onClick={() => {
                        deleteuser(user.email);
                      }}
                    />{' '}
                  </span>
                </span>
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};

export default Users;
