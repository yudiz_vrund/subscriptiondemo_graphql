import React, { useState, useEffect } from 'react';
import { GET_MESSAGES } from '../GraphQL/Queries';
import { useMutation, useSubscription } from '@apollo/client';
import { SEND_MSG } from '../GraphQL/Mutations';
import { NEW_MESSAGE_SUBSCRIPTION } from '../GraphQL/Subscription';
import { ToastContainer, toast } from 'react-toastify';

const Sendmsg = () => {
  const [msg, setMsg] = useState('');
  const [senderEmail, setSenderEmail] = useState('');
  const [receiverEmail, setReceiverEmail] = useState('');

  const [SendMsg] = useMutation(SEND_MSG);
  const { data: newmsgsub } = useSubscription(NEW_MESSAGE_SUBSCRIPTION, {
    variables: {
      newMessageReceiverMail: receiverEmail,
    },
  });

  const sendMsg = (e) => {
    e.preventDefault();
    if (msg && senderEmail && receiverEmail) {
      SendMsg({
        variables: {
          createMessageMessage: msg,
          createMessageSenderMail: senderEmail,
          createMessageReceiverMail: receiverEmail,
        },
        refetchQueries: [{ query: GET_MESSAGES }],
      });
      e.target.reset();
    } else {
      alert('Please fill the details');
    }
  };

  useEffect(() => {
    const data = newmsgsub?.newMessage;
    if (data != null) {
      toast(
        <div>
          <p>{data.senderMail}</p>
          <p className="text-success">{data.message}</p>
          <p>{data.receiverMail}</p>
        </div>,
        {
          position: 'bottom-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: 1,
        }
      );
    }
  }, [newmsgsub]);

  return (
    <>
      <div className="col-md-6 py-3 border">
        <ToastContainer autoClose={3000} />
        <h4 class="pb-2">Send Message</h4>
        <form onSubmit={sendMsg}>
          <input
            type="text"
            placeholder="Sender Email"
            onChange={(e) => {
              setSenderEmail(e.target.value);
            }}
            className="form-control mb-3"
          />
          <input
            type="text"
            placeholder="Receiver Email"
            onChange={(e) => {
              setReceiverEmail(e.target.value);
            }}
            className="form-control mb-3"
          />
          <input
            type="text"
            placeholder="Message"
            onChange={(e) => {
              setMsg(e.target.value);
            }}
            className="form-control mb-3"
          />
          <button className="btn btn-outline-primary">Send</button>
        </form>
      </div>
    </>
  );
};

export default Sendmsg;
