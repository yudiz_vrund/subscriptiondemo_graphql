import React, { useState, useEffect } from 'react';
import { CREATE_USER } from '../GraphQL/Mutations';
import { useMutation } from '@apollo/client';
import { GET_USERS } from '../GraphQL/Queries';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { NEW_USER_SUBSCRIPTION } from '../GraphQL/Subscription';
import { useSubscription } from '@apollo/client';

export const Createuser = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const [AddUser] = useMutation(CREATE_USER);

  const { data: newusersub } = useSubscription(NEW_USER_SUBSCRIPTION);

  const newuser = newusersub?.newUser;

  useEffect(() => {
    if (newuser != null) {
      toast(
        <div>
          <p>{newuser.name}</p>
          <p>{newuser.email}</p>
        </div>,
        {
          position: 'bottom-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: 1,
        }
      );
    }
  }, [newuser]);

  const createuser = (e) => {
    e.preventDefault();
    if (name && email) {
      AddUser({
        variables: {
          createUserName: name,
          createUserEmail: email,
        },

        refetchQueries: [{ query: GET_USERS }],
      });
      e.target.reset();
      setName('');
      setEmail('');
    } else {
      alert('Please fill the details');
    }
  };

  return (
    <>
      <div className="col-md-5 py-3 border">
        <ToastContainer />
        <h4 class="pb-2">Create User</h4>
        <form onSubmit={createuser}>
          <input
            type="text"
            placeholder="Name"
            onChange={(e) => {
              setName(e.target.value);
            }}
            className="form-control mb-3"
          />
          <input
            type="text"
            placeholder="Email"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            className="form-control mb-3"
          />
          <button className="btn btn-outline-primary">Submit</button>
        </form>
      </div>
    </>
  );
};
